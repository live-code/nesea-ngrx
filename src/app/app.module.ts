import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { routerReducer, RouterReducerState, RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { RouterEffects } from './core/router/router.effects';
import { authReducer, AuthState } from './core/auth/store/auth.reducer';
import { AuthEffects } from './core/auth/store/auth.effects';
import { NavbarComponent } from './core/components/navbar.component';
import { AuthInterceptor } from './core/auth/auth.interceptor';
import { IfLoggedDirective } from './core/auth/if-logged.directive';

export interface AppState {
  autentication: AuthState;
  router: RouterReducerState;
}
export const reducers: ActionReducerMap<AppState> = {
  autentication: authReducer,
  router: routerReducer
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    IfLoggedDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 20,
    }),
    EffectsModule.forRoot([RouterEffects, AuthEffects]),
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Minimal
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: AuthInterceptor
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
