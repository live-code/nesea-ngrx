import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { logout } from '../auth/store/auth.actions';
import { Observable } from 'rxjs';
import { getIsLogged } from '../auth/store/auth.selectors';

@Component({
  selector: 'app-navbar',
  template: `
    <button routerLink="login" >login</button>
    <button routerLink="pexels">pexels</button>
    <button routerLink="items" *appIfLogged="'admin'" >items</button>
    <button *appIfLogged  (click)="logoutHandler()">logout</button>
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {
  isLogged$: Observable<boolean> = this.store.pipe(select(getIsLogged));

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
  }

  logoutHandler(): void {
    this.store.dispatch(logout())
  }
}
