import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { getIsLogged } from './store/auth.selectors';
import { tap } from 'rxjs/operators';
import { go } from '../router/router.actions';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  canActivate(): Observable<boolean> {
    return this.store.pipe(
      select(getIsLogged),
      tap(isLogged => {
        if (!isLogged) {
          this.store.dispatch(go({ path: 'login'}))
        }
      })
    )
  }

  constructor(private store: Store<AppState>) {
  }
}
