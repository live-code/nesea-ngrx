import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { iif, Observable, of, throwError } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { getAuthToken } from './store/auth.selectors';
import { catchError, mergeMap, take } from 'rxjs/operators';
import { go } from '../router/router.actions';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private store: Store<AppState>) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.store.select(getAuthToken)
      .pipe(
        take(1),
        mergeMap(token => iif(
          () => !!token,
          next.handle(request.clone({ setHeaders: { Authorization: 'Bearer ' + token} }) ),
          next.handle(request)
        )),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 401:
              case 404:
              default:
                this.store.dispatch(go({ path: 'login'}));
            }
          }
          return throwError(err)
        })
      );
  }
}
