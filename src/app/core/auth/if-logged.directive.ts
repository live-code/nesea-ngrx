import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { getIsLogged, getRole } from './store/auth.selectors';
import { distinctUntilChanged, filter, takeUntil, tap, withLatestFrom } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';

@Directive({
  selector: '[appIfLogged]'
})
export class IfLoggedDirective implements OnDestroy, OnInit {
  @Input() appIfLogged: string;
  destroy$: Subject<any> = new Subject<any>();

  constructor(
    private view: ViewContainerRef,
    public template: TemplateRef<any>,
    private store: Store<AppState>
  ) {
  }

  ngOnInit(): void {
    this.store.pipe(
      select(getIsLogged),
      takeUntil(this.destroy$),
      tap(() => this.view.clear()),
      distinctUntilChanged(),
      withLatestFrom(
        this.store.pipe(select(getRole))
      ),
      filter(([isLogged, role]) => {
        return (isLogged && !this.appIfLogged) ||
               (isLogged && role === this.appIfLogged)
      })
    )
      .subscribe(() => {
        this.view.createEmbeddedView(this.template);
      });
  }
  ngOnDestroy(): void {
    this.destroy$.next();
  }

}
