import { Auth } from '../auth';
import { createReducer, on } from '@ngrx/store';
import * as AuthActions from './auth.actions';

export interface AuthState {
  auth: Auth;
  error: boolean;
}

export const initialState: AuthState = {
  auth: null,
  error: false
};

export const authReducer = createReducer(
  initialState,
  on(AuthActions.syncWithLocalStorage, (state, action) => ({ auth: action.auth, error: false})),
  on(AuthActions.loginSuccess, (state, action) => ({ auth: action.auth, error: false})),
  on(AuthActions.loginFailed, (state) => ({ ...state, error: true})),
  on(AuthActions.logout, () => ({ auth: null, error: false})),
)
