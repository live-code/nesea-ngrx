import { AppState } from '../../../app.module';

export const getIsLogged = (state: AppState) => !!state.autentication.auth;
export const getAuthToken = (state: AppState) => state.autentication.auth?.token;
export const getAuthError = (state: AppState) => state.autentication.error;
export const getRole = (state: AppState) => state.autentication.auth?.role;

