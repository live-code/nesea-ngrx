import { createAction, props } from '@ngrx/store';
import { Auth } from '../auth';

export const login = createAction(
  '[auth] login',
  props<{ email: string, password: string}>()
);

export const syncWithLocalStorage = createAction(
  '[auth] sync with localstorage',
  props<{ auth: Auth}>()
);

export const loginSuccess = createAction(
  '[auth] login success',
  props<{ auth: Auth}>()
);

export const loginFailed = createAction(
  '[Auth] Login Failed'
);

export const logout = createAction('[Auth] Logout');
