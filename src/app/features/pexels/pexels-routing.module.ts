import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PexelsComponent } from './pexels.component';

const routes: Routes = [{ path: '', component: PexelsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PexelsRoutingModule { }
