import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PexelsRoutingModule } from './pexels-routing.module';
import { PexelsComponent } from './pexels.component';


@NgModule({
  declarations: [PexelsComponent],
  imports: [
    CommonModule,
    PexelsRoutingModule
  ]
})
export class PexelsModule { }
