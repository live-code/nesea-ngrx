import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { go } from '../../core/router/router.actions';
import { login } from '../../core/auth/store/auth.actions';
import { NgForm } from '@angular/forms';
import { Credentials } from '../../core/auth/auth';

@Component({
  selector: 'app-login',
  template: `
    <form #f="ngForm" (submit)="signinHandler(f.value)">
      <input type="text" ngModel name="email" required>
      <input type="text" ngModel name="password" required>
      <button type="submit" [disabled]="f.invalid">SEND</button>
    </form>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor(private store: Store<AppState>) {
  /*  setTimeout(() => {
      store.dispatch(go({ path: 'pexels' }))
    }, 2000)*/
  }

  ngOnInit(): void {
  }

  signinHandler({ email, password }): void {
    this.store.dispatch(login({ email, password}))
  }
}
