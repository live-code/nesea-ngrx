import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ItemsState, reducer } from './store/items.reducer';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './store/items.effects';
import { itemReducer, ItemState } from './store/item.reducer';

export interface UsersState {
  items: ItemsState;
  active: ItemState;
  config: number;
}
export const reducers: ActionReducerMap<UsersState> = {
  items: reducer,
  active: itemReducer,
  config: () => 123
};

@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    FormsModule,
    UsersRoutingModule,
    StoreModule.forFeature('users', reducers),
    EffectsModule.forFeature([ ItemsEffects ])
  ]
})
export class UsersModule { }
