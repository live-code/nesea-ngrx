import { Component, OnInit, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
import { Actions } from '@ngrx/effects';
import { getItem, getItems, getItemsError, getItemsPending } from './store/items.selectors';
import { Item } from '../../model/item';
import { addItem, deleteItem, loadItems } from './store/items.actions';
import { ItemsEffects } from './store/items.effects';
import { AppState } from '../../app.module';
import { resetActive, setActive } from './store/item.action';

@Component({
  selector: 'app-items',
  template: `
    <div *ngIf="pending$ | async">loading....</div>
    <div *ngIf="hasError$ | async">Errore!!!</div>
    <form #f="ngForm" (submit)="addHandler(f)">
      <input type="text" ngModel name="name">
    </form>
    <li *ngFor="let item of items$ | async" (click)="showDetails(item.id)">
      {{item.name}}
      <button (click)="deleteHandler(item)">delete</button>
    </li>

    <pre *ngIf="items$ | async">user: {{item$ | async | json}}</pre>
    <button (click)="closeModal()">reset</button>
  `,
})
export class UsersComponent implements OnInit {
  @ViewChild('f') form: NgForm;
  items$: Observable<Item[]> = this.store.pipe(select(getItems));
  item$: Observable<Item> = this.store.pipe(select(getItem));
  hasError$: Observable<boolean> = this.store.pipe(select(getItemsError));
  pending$: Observable<boolean> = this.store.pipe(select(getItemsPending));

  constructor(private store: Store<AppState>, private actions$: Actions, private itemsEffects: ItemsEffects) {
    itemsEffects.addItem$.subscribe(() => this.form.reset());
  }

  ngOnInit(): void {
    this.store.dispatch(loadItems());
  }

  addHandler(form: NgForm): void {
    this.store.dispatch(addItem({ item: form.value }));

  }

  deleteHandler(item: Item): void {
    this.store.dispatch(deleteItem({ id: item.id  }));
  }

  showDetails(id: number): void {
    this.store.dispatch(setActive({ id }))
  }

  closeModal(): void {
    this.store.dispatch(resetActive())
  }
}
