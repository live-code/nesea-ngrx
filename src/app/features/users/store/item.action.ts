import { createAction, props } from '@ngrx/store';
import { Item } from '../../../model/item';

export const setActive = createAction(
  '[Item] set active',
  props<{ id: number }>()
);

export const resetActive = createAction(
  '[Item] set reset',
);
