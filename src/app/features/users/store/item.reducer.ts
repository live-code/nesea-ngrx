import { createReducer, on } from '@ngrx/store';
import { resetActive, setActive } from './item.action';


export interface ItemState {
  id: number;
}

export const INITIAL_STATE: ItemState = {
  id: null
};

export const itemReducer = createReducer(
  INITIAL_STATE,
  on(setActive, (state, action) => ({ id: action.id})),
  on(resetActive, (state) => ({ id: null})),
);

