import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  addItem, addItemFail,
  addItemSuccess,
  deleteItem,
  deleteItemFail,
  deleteItemSuccess,
  loadItems,
  loadItemsFails,
  loadItemsSuccess
} from './items.actions';
import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Item } from '../../../model/item';
import { of } from 'rxjs';

of([1, 2, 3])
  .pipe(
    switchMap(items => items),
    map(item => item * 2)
  )
  .subscribe(console.log)



@Injectable()
export class ItemsEffects {
  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(loadItems),
    switchMap(
      () => this.http.get<Item[]>('http://localhost:3000/users')
        .pipe(
          map(items => loadItemsSuccess({ items })),
          catchError(() => of(loadItemsFails()))
        )
    )
  ));


  deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(deleteItem),
    switchMap(
      ({ id }) => this.http.delete<void>('http://localhost:3000/users/' + id)
        .pipe(
          map(() => deleteItemSuccess({ id })),
          catchError(() => of(deleteItemFail()))
        )
      )
  ));

  addItem$ = createEffect(() => this.actions$.pipe(
    ofType(addItem),
    mergeMap(
      action => this.http.post<Item>('http://localhost:3000/users', action.item)
        .pipe(
          map((item) => addItemSuccess({ item })),
          catchError(() => of(addItemFail()))
        )
      )
  ));


  constructor(private actions$: Actions, private http: HttpClient) {
  }
}
