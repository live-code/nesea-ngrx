import { createAction, props } from '@ngrx/store';
import { Item } from '../../../model/item';

export const loadItems = createAction(
  '[Items] load'
);
export const loadItemsSuccess = createAction(
  '[Items] load success',
  props<{ items: Item[] }>()
);
export const loadItemsFails = createAction(
  '[Items] load fails'
);


export const addItem = createAction(
  '[Items] add',
  props<{ item: Item }>()
);

export const addItemSuccess = createAction(
  '[Items] add success',
  props<{ item: Item }>()
);

export const addItemFail = createAction(
  '[Items] add failed',
);


export const deleteItem = createAction(
  '[Items] delete',
  props<{ id: number }>()
);

export const deleteItemSuccess = createAction(
  '[Items] Delete Success',
  props<{ id: number }>()
);

export const deleteItemFail = createAction(
  '[Items] Delete failed',
);
