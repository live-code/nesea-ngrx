import { UsersState } from '../users.module';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Item } from '../../../model/item';

export const getUsersFeature = createFeatureSelector<UsersState>('users');


export const getItems = createSelector(
  getUsersFeature,
  // ... //
  (state: UsersState, /***/) => state.items.list
);

export const getActiveId = createSelector(
  getUsersFeature,
  state => state.active?.id
);

export const getItem = createSelector(
  getItems,
  getActiveId,
  (state: Item[], id: number) => state.find(item => item.id === id)
);


export const getItemsError = createSelector(
  getUsersFeature,
  state => state.items.hasError
);
export const getItemsPending = createSelector(
  getUsersFeature,
  state => state.items.pending
);
