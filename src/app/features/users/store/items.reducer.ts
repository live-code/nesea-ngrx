import { createReducer, on } from '@ngrx/store';
import {
  addItem,
  addItemFail,
  addItemSuccess,
  deleteItem,
  deleteItemFail,
  deleteItemSuccess, loadItems,
  loadItemsFails,
  loadItemsSuccess
} from './items.actions';
import { Item } from '../../../model/item';


export interface ItemsState {
  list: Item[];
  hasError: boolean;
  pending: boolean;
}

export const INITIAL_STATE: ItemsState = {
  list: [],
  hasError: false,
  pending: false,
};

export const reducer = createReducer(
  INITIAL_STATE,
  on(loadItems, (state, action) => ({...state, pending: true})),
  on(loadItemsSuccess, (state, action) => ({...state, list: [...action.items], pending: false})),
  on(loadItemsFails, (state, action) => ({ ...state, hasError: true })),

  on(addItem, (state, action) => ({ ...state, hasError: false, pending: true })),
  on(addItemSuccess, (state, action) => ({ ...state, list: [...state.list, action.item], hasError: false, pending: false })),
  on(addItemFail, (state, action) => ({ ...state, hasError: true, pending: false })),

  on(deleteItem, (state, action) => ({ ...state, hasError: false, pending: true })),
  on(deleteItemSuccess, (state, action) => ({ list: state.list.filter(item => item.id !== action.id), hasError: false, pending: false })),
  on(deleteItemFail, (state, action) => ({ ...state, hasError: true, pending: false }))
);

